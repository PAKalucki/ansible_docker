FROM centos:centos7

RUN yum -y update && \
    yum -y install epel-release && \
    yum -y install ansible PyYAML python-jinja2 python-httplib2 python-keyczar python-paramiko python-setuptools git python-pip && \
    yum clean all

RUN pip install --upgrade pip && pip install pywinrm

RUN mkdir /opt/workspace

WORKDIR /opt/workspace